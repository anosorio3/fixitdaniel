import java.io.File;
import java.lang.reflect.Constructor;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class fixItGUI extends Application {

	private static Image[][] sprites = RecolectorDeImagenes.getAllSprites();
	static Daniel daniel = new Daniel();
	static Ralph ralph = new Ralph();
	static ImageView dan = new ImageView();
	static ImageView ral =new ImageView();
	private ImageView ladrillo = new ImageView();

	private long lf;
	private Interfaz model;
	private GridPane Gpane;
	private Pane pane;
	private Label lblStatus;
	private ImageView[][] imageBoard;

	public fixItGUI() {
		super();
	}

	@Override
	public void init() throws Exception {
		super.init();
		String className = getParameters().getUnnamed().get(0);
		String random =""+ (int) (Math.random()*4+1);
		String filename = getParameters().getUnnamed().get(1);
		filename=filename.replace("1", random);
		System.out.printf("className:%s, fileName:%s%n", className, filename);
		int [][] boardW = metodosParaGUI.loadBoard(filename);
		filename = getParameters().getUnnamed().get(2);
		filename=filename.replace("1", random);
		System.out.printf("className:%s, fileName:%s%n", className, filename);
		int [][] boardO = metodosParaGUI.loadBoard(filename);
		Constructor<?> c = Class.forName(className).getDeclaredConstructor(boardW.getClass(),boardO.getClass(),daniel.getClass(),sprites.getClass(),dan.getClass());
		c.setAccessible(true);
		model = (Interfaz) c.newInstance(new Object[] {boardW,boardO,daniel,sprites,dan});

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		String path = "Sounds/BG.mp3";
		Media media = new Media(new File(path).toURI().toString());
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		mediaPlayer.setAutoPlay(true);
		MediaView mediaView = new MediaView(mediaPlayer);
	
		
		Gpane = new GridPane();
		MenuBar mb = new MenuBar();
		Menu menu = new Menu("Opciones");
		menu.getItems().add(new MenuItem("Reinicar"));
		mb.getMenus().add(menu);
		
		menu.setOnAction(e->{
			try {
				init();
				daniel.vivo = true;
				refreshGPane(model.getBoardWindows(),model.getBoardObstacles());
				refreshPane();
				
			} catch (Exception e1) {
				
				e1.printStackTrace();
			}
			
		});
		pane = new Pane();
		metodosParaGUI.cargarImagenes(dan,daniel,ral,ralph,ladrillo,sprites);
		int [][] boardW = model.getBoardWindows();
		int [][] boardO = model.getBoardObstacles();
		imageBoard = new ImageView[boardW.length][boardW[0].length];
		for (int i=0; i<boardW.length; i++) {
			for (int j=0; j<boardW[i].length; j++) {
				imageBoard[i][j] = new ImageView(getImageFrom(boardW[i][j],boardO[i][j])); 
				Gpane.add(imageBoard[i][j], j, i);
			}
		}
		//		Gpane.setBackground(new Background(new BackgroundFill(Color.valueOf("#000000"),CornerRadii.EMPTY,Insets.EMPTY)));
		pane.getChildren().addAll(ladrillo,ral,dan);
		refreshPane();

		
		Thread IAralph = new Thread(()->{
			while(ralph.sigueJugando(model.hasFinalized())||ralph.sigueJugando(model.hasDied())) {
				int x = Ralph.getNewX();
				int actualX= ralph.getX();
				if(actualX!=x) {
					boolean iterador = false;
					byte z;
					if(actualX<x)
						z=1;
					else
						z=-1;
					for (int i = actualX; i != x; i+=z*2, iterador=!iterador) {
						if(iterador)
							if(z>0)
								ral.setImage(sprites[5][3]);
							else
								ral.setImage(sprites[5][5]);
						else 
							if(z>0)
								ral.setImage(sprites[5][4]);
							else
								ral.setImage(sprites[5][6]);
						try {
							Thread.sleep(100);
						} catch (InterruptedException e1) {
						}
						ral.setTranslateX(i);
						
					}
					ralph.setX(x);
					hilos.ladrillo(daniel,dan,ladrillo,ralph,sprites);
					for (int i = 0; i <= 5; i+=1, iterador=!iterador) {
						if(iterador)
							ral.setImage(sprites[5][1]);
						else 
							ral.setImage(sprites[5][2]);
						try {
							Thread.sleep(500);
						} catch (InterruptedException e1) {
						}
					}
					ral.setImage(sprites[5][0]);
				}
				try {
					Thread.sleep(700);
				} catch (InterruptedException e1) {
				}
			}
			
		});
		
		IAralph.start();


		lf = System.currentTimeMillis();
		Gpane.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				long lo = System.currentTimeMillis();
				if(lo-lf>400) {
					lf=lo;
					KeyCode code = event.getCode();
					//System.out.println(model.hasFinalized());
					if (model.hasFinalized()) {
						lblStatus.setText("¡GANASTE! Reconstruiste las ventanas!");
						IAralph.stop();
						return;
					}
					if (model.hasDied()) {
						lblStatus.setText("¡PERDISTE! Hail Ralph");
						IAralph.stop();
						return;
					}
					if (code == KeyCode.RIGHT) {
						model.moveRight();
					} else if (code == KeyCode.LEFT) {
						model.moveLeft();
					} else if (code == KeyCode.DOWN) {
						model.moveDown();
					} else if (code == KeyCode.UP) {
						model.moveUp();
					}
					else if (code == KeyCode.CONTROL) {
						model.reparar();
					}

					
					System.out.println(daniel.getX()+", "+daniel.getY());
					refreshGPane(model.getBoardWindows(),model.getBoardObstacles());
					refreshPane();

				}
			}
		});




		lblStatus = new Label("Jugando...");
		lblStatus.setTextFill(Color.WHITE);
		BorderPane borderPane = new BorderPane();
		Group panes=new Group(Gpane,pane);
		borderPane.setCenter(panes);
		borderPane.setTop(mb);
		borderPane.setBottom(lblStatus);
		borderPane.setBackground(new Background(new BackgroundImage
				(sprites[6][1], BackgroundRepeat.NO_REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT)));


		Scene scene = new Scene(borderPane,630,658);
		primaryStage.setScene(scene);
		primaryStage.getIcons().add(sprites[6][0]);
		primaryStage.setTitle("Fix It Daniel!");
		primaryStage.show();

		Gpane.requestFocus();
	}

	protected void refreshGPane(int[][] boardW,int[][] boardO) {
		for (int i=0; i<boardW.length; i++) {
			for (int j=0; j<boardW[i].length; j++) {
				imageBoard[i][j].setImage(getImageFrom(boardW[i][j],boardO[i][j]));
			}
		}
		Gpane.requestFocus();
	}

	protected void refreshPane() {
		//		daniel.pos[0]=(int)dan.getTranslateX();
		//		daniel.pos[1]=(int)dan.getTranslateY();
		pane.requestFocus();
		Gpane.requestFocus();
	}



	private Image getImageFrom(int n1, int n2) {
		return sprites[n1][n2];
	}

	public static void main(String[] args) {
		Application.launch(fixItGUI.class, "danielModel", "levels/level1W.txt","levels/level1O.txt");
	}

}
