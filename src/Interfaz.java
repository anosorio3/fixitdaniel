import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public interface Interfaz {

	public int[][] getBoardWindows();

	public int[][] getBoardObstacles();

	public void moveRight();

	public void moveLeft();

	public void moveUp();

	public void moveDown();

	public void reparar();

	public boolean hasFinalized();
	
	public boolean hasDied();

	


}