import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import javafx.scene.image.Image;

class RecolectorDeImagenes {

	static Image[][] getAllSprites(){
		Image[][] sprites= new Image[7][8];
		try {
			sprites[0]=getImages("0V");
		} catch (Exception e) {
			System.out.println("Error en 0V");
		}
		try {
			sprites[1]=getImages("1V");
		} catch (Exception e) {
			System.out.println("Error en 1V");
		}
		try {
			sprites[2]=getImages("2V");
		} catch (Exception e) {
			System.out.println("Error en 2V");
		}
		try {
			sprites[3]=getImages("0D");
		} catch (Exception e) {
			System.out.println("Error en 0D");
		}
		try {
			sprites[4]=getImages("1D");
		} catch (Exception e) {
			System.out.println("Error en 1D");
		}
		try {
			sprites[5]=getImages("0R");
		} catch (Exception e) {
			System.out.println("Error en 0R");
		}
		try {
			sprites[6]=getImages("aux");
		} catch (Exception e) {
			System.out.println("Error en br");
		}
		return sprites;
	}


	static Image[] getImages(String x) throws Exception {
		File dir= new File("sprites/");
		System.out.println(dir.getAbsolutePath());
		System.out.println(dir.isDirectory());
		File[] imageFiles =dir.listFiles((d,name)->name.startsWith(x));
		for(File f:imageFiles)
			System.out.println(f.getName());
		Arrays.sort(imageFiles,(f1,f2)->{
			int n1= Integer.parseInt(f1.getName().replaceAll("[^0-9]", ""));
			int n2= Integer.parseInt(f2.getName().replaceAll("[^0-9]", ""));
			return n1-n2;
		});
		Image[] images = new Image[7];
		for (int i = 0; i < imageFiles.length; i++) 
			images[i]=new Image(new FileInputStream(imageFiles[i]));
		return images;


	}
}
