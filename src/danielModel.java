import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class danielModel implements Interfaz {

	private int [][] boardW;

	private int [][] boardO;

	private int rows;

	private int cols;

	private static Image[][] sprites;
	
	private static ImageView dan;
	
	private Daniel daniel;
	

	public danielModel(int [][] boardW, int [][] boardO, Daniel daniel,
			Image[][] sprites, ImageView dan) {
		if (boardW==null||boardO==null)
			throw new IllegalArgumentException("Una de las matrices est� vacia");
		if(boardW.length!=boardO.length||boardO[0].length!=boardW[0].length)
			throw new IllegalArgumentException("Las matrices no coinciden");
		this.boardW = boardW;
		this.boardO = boardO;
		cols=boardW[0].length;
		rows=boardW.length;
		this.daniel=daniel;
		this.sprites=sprites;
		this.dan=dan;
	}


	@Override
	public int[][] getBoardWindows() {

		return boardW;
	}

	@Override
	public int[][] getBoardObstacles() {
		
		return boardO;
	}

	@Override
	public void moveRight() {
		int x=calcularCoord.getX_V(daniel)+1;
		int y=calcularCoord.getY_V(daniel);
		if(x<boardO[0].length) {
			if(boardO[y][x]>1) 
					return;
		}
		else if(x*70>=(boardO[0].length-1)*70)
			return;
		hilos.right(dan, sprites);
		daniel.pos[0]=(int)dan.getTranslateX()+70;


	}

	@Override
	public void moveLeft() {
		int x=calcularCoord.getX_V(daniel);
		int y=calcularCoord.getY_V(daniel);
		if(x>0) {
			if(boardO[y][x]>1) 
					return;
		}
		else if(daniel.getX()-70<0)
			return;
		hilos.left(dan, sprites);
		daniel.pos[0]=(int)dan.getTranslateX()-70;

	}
	

	@Override
	public void moveUp() {
		int x=calcularCoord.getX_V(daniel);
		int y=calcularCoord.getY_V(daniel)-1;
		if(y<0)
			return;

		if(boardO[y][x]==1||boardO[y][x]==3)
			return;
		if(daniel.getX()%70>20) {
			if((calcularCoord.getX_V(daniel)*70+35>daniel.getX()+35)
					&&(boardO[y][x]==2||boardO[y][x]==3)) {
				return;
			}
			if((calcularCoord.getX_V(daniel)*70+35<daniel.getX()+35)
					&&(boardO[y][x+1]==2||boardO[y][x+1]==3))
				return;
		}
		hilos.up(dan,sprites);
		daniel.pos[1]=daniel.getY()-128;
	}

	@Override
	public void moveDown() {
		int x=calcularCoord.getX_V(daniel);
		int y=calcularCoord.getY_V(daniel)+1;
		if(y>=boardO.length)
			return;

		if(boardO[y-1][x]==1||boardO[y-1][x]==3)
			return;
		if(daniel.getX()%70>20) {
			if((calcularCoord.getX_V(daniel)*70+35>daniel.getX()+35)
					&&(boardO[y][x]==2||boardO[y][x]==3)) {
				return;
			}
			if((calcularCoord.getX_V(daniel)*70+35<daniel.getX()+35)
					&&(boardO[y][x+1]==2||boardO[y][x+1]==3))
				return;
		}
		hilos.down(dan,sprites);
		daniel.pos[1]=daniel.getY()+128;
	}

	
	@Override
	public void reparar() {
		int x = calcularCoord.getX_V(daniel);
		int y = calcularCoord.getY_V(daniel);
		if(boardW[y][x]!=0) {
			hilos.reparar(dan, sprites);
			boardW[y][x]-=1;
		}

	}

	@Override
	public boolean hasFinalized() {
		for (int i = 0; i < boardW.length; i++) {
			for (int j = 0; j < boardW[0].length; j++) {
				if(boardW[i][j]!=0)
					return false;
			}
		}
		return true;
	}

	@Override
	public boolean hasDied() {
		return !daniel.vivo;
	}










}
