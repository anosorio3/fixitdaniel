import java.io.File;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

public class hilos {
	static void reparar(ImageView dan, Image[][] sprites){
		Thread hilo = new Thread(()-> {
			Image original = dan.getImage();
			if(original.equals(sprites[3][0]))
				dan.setImage(sprites[3][3]);
			else
				dan.setImage(sprites[4][3]);
			try {
				Thread.sleep(300);
			} catch (InterruptedException e1) {
			}
			dan.setImage(original);
			String path = "Sounds/fix.wav";
			Media media = new Media(new File(path).toURI().toString());
			MediaPlayer mediaPlayer = new MediaPlayer(media);
			mediaPlayer.setAutoPlay(true);
			MediaView mediaView = new MediaView(mediaPlayer);
		
		});
		hilo.start();
	}

	static void left(ImageView dan, Image[][] sprites){
		int x= (int)dan.getTranslateX();

		Thread hilo = new Thread(()-> {
			boolean iterador=true;
			for (int i = x; i >= x-70; i-=14, iterador=!iterador) {
				if(iterador)
					dan.setImage(sprites[4][1]);
				else 
					dan.setImage(sprites[4][2]);
				try {
					Thread.sleep(50);
				} catch (InterruptedException e1) {
				}
				dan.setTranslateX(i);

			}
			dan.setImage(sprites[4][0]);
		});
		hilo.start();
	}

	static void right(ImageView dan, Image[][] sprites){
		int x= (int)dan.getTranslateX();

		Thread hilo = new Thread(()-> {
			boolean iterador=true;
			for (int i = x; i <= x+70; i+=14, iterador=!iterador) {
				if(iterador)
					dan.setImage(sprites[3][1]);
				else 
					dan.setImage(sprites[3][2]);
				try {
					Thread.sleep(50);
				} catch (InterruptedException e1) {
				}
				dan.setTranslateX(i);

			}
			dan.setImage(sprites[3][0]);
		});
		hilo.start();
	}

	static void up(ImageView dan, Image[][] sprites) {
		int y= (int)dan.getTranslateY();
		int x;
		if(dan.getImage().equals(sprites[3][0]))
			x=3;
		else
			x=4;
		Thread hilo = new Thread(()-> {
			for (int i = y; i >= y-128; i-=4 ) {
				dan.setImage(sprites[x][1]);
				try {
					Thread.sleep(10);
				} catch (InterruptedException e1) {
				}
				dan.setTranslateY(i);

			}
			dan.setImage(sprites[x][0]);
		});
		String path = "Sounds/jump.wav";
		Media media = new Media(new File(path).toURI().toString());
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		mediaPlayer.setAutoPlay(true);
		MediaView mediaView = new MediaView(mediaPlayer);
	
		hilo.start();

	}

	static void down(ImageView dan, Image[][] sprites) {
		int y= (int)dan.getTranslateY();
		int x;
		if(dan.getImage().equals(sprites[3][0]))
			x=3;
		else
			x=4;
		Thread hilo = new Thread(()-> {
			for (int i = y; i <= y+128; i+=4 ) {
				dan.setImage(sprites[x][1]);
				try {
					Thread.sleep(10);
				} catch (InterruptedException e1) {
				}
				dan.setTranslateY(i);

			}
			dan.setImage(sprites[x][0]);
		});
		hilo.start();

	}

	public static void ladrillo(Daniel daniel, ImageView dan, ImageView ladrillo, Ralph ralph, Image[][] sprites) {
		int x= (int) ralph.getX();
		ladrillo.setTranslateX(x);
		ladrillo.setVisible(true);
		Thread hilo = new Thread(()-> {
			for (int i = 64; i <= 640; i+=4 ) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e1) {
				}
				if(daniel.getX()==x) {
					if(i+128>dan.getTranslateY()&&i<dan.getTranslateY()) {
						daniel.pierdeVida();
						muerte(dan, sprites);
					}
				}
				ladrillo.setTranslateY(i);

			}
			ladrillo.setVisible(false);
		});
		hilo.start();
		
	}

	private static void muerte(ImageView dan, Image[][] sprites) {
		dan.setImage(sprites[3][4]);
		try {
			Thread.sleep(200);
		} catch (InterruptedException e1) {
		}
		dan.setImage(sprites[4][4]);
		String path = "Sounds/pierde.wav";
		Media media = new Media(new File(path).toURI().toString());
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		mediaPlayer.setAutoPlay(true);
		MediaView mediaView = new MediaView(mediaPlayer);
	
		
		
	}




}
