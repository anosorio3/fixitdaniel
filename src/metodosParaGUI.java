import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class metodosParaGUI {
	
	
	static int[][] loadBoard(String filename)  {
		int [][] board = null;
		try (BufferedReader in = new BufferedReader(new FileReader(filename))) {
			List<String> lines = new ArrayList<String>();
			for (String line; (line=in.readLine())!=null; ) {
				lines.add(line);
			}
			board = new int[lines.size()][lines.get(0).length()];
			for (int i=0; i<board.length; i++) {
				char[] aux = lines.get(i).toCharArray(); 
				for (int j = 0; j < board.length; j++) {
					board[i][j]=aux[j]-48;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return board;
	}

	public static void cargarImagenes(ImageView dan, Daniel daniel, ImageView ral, Ralph ralph, ImageView ladrillo, Image[][] sprites) {
		ladrillo.setImage(sprites[6][2]);
		ladrillo.setVisible(false);
		dan.setImage(sprites[3][0]);
		dan.setTranslateX(daniel.getX());
		dan.setTranslateY(daniel.getY());
		ral.setImage(sprites[5][0]);
		ral.setTranslateX(ralph.getX());
	}
	
	
}
