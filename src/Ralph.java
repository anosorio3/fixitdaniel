
public class Ralph {

	static int pos;
	static boolean sigueJugando;
	
	Ralph(){
		pos=140;
		sigueJugando=true;
	}
	
	static int getX() {
		return pos;
	}
	
	static int getNewX() {
		return getRandomWindow()*70;
	}
	
	static private int getRandomWindow() {
		return (int) (Math.random()*5);
	}

	public void setX(int x) {
		pos=x;
		
	}

	public boolean sigueJugando(boolean hasFinalized) {
		sigueJugando=!hasFinalized;
		return sigueJugando;
	}
	
}
